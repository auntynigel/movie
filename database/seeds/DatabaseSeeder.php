<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // $this->call(UsersTableSeeder::class);
       // $this->call(AgesTableSeeder::class);       
       // $this->call(ActorsTableSeeder::class);
       // $this->call(GenresTableSeeder::class);
       // $this->call(FilmsTableSeeder::class);
       // $this->call(ActorFilmsTableSeeder::class);
       // $this->call(AgeFilmsTableSeeder::class);
       $this->call(FilmGenresTableSeeder::class);
    }
}
