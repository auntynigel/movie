<?php

use Illuminate\Database\Seeder;

class AgesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ages')->insert([
            ['id' => 1, 'ageRating' => 'U'],
            ['id' => 2, 'ageRating' => 'PG'],
            ['id' => 3, 'ageRating' => '12'],
            ['id' => 4, 'ageRating' => '15'],
            ['id' => 5, 'ageRating' => '18'],
        ]);
    }
}
