<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->insert([
            ['id' => 1, 'genre' => 'Action'],
            ['id' => 2, 'genre' => 'Adventure'],
            ['id' => 3, 'genre' => 'Comedy'],
            ['id' => 4, 'genre' => 'Drama'],
            ['id' => 5, 'genre' => 'Historical'],
            ['id' => 6, 'genre' => 'SciFi'],
            ['id' => 7, 'genre' => 'War'],
            ['id' => 8, 'genre' => 'Biography'],
            ['id' => 9, 'genre' => 'Animated'],
            ['id' => 10, 'genre' => 'Musical'],
            ['id' => 11, 'genre' => 'Horror'],
            ['id' => 12, 'genre' => 'Sports'],
            ['id' => 13, 'genre' => 'Crime'],
            ['id' => 14, 'genre' => 'Fantasy'],
            ['id' => 15, 'genre' => 'Sports'],
            ['id' => 16, 'genre' => 'Thriller'],
        ]);
    }
}
