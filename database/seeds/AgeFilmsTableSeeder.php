<?php

use Illuminate\Database\Seeder;

class AgeFilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('age_films')->insert([
            ['id' => 1, 'age_id' => 3, 'film_id' => 2],
            ['id' => 2, 'age_id' => 5, 'film_id' => 3],
        ]);
    }
}
