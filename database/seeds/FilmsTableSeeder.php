<?php

use Illuminate\Database\Seeder;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('films')->insert([
            ['id' => 2, 'title' => 'RED', 'tagline' => 'Still Armed. Still Dangerous. Still Got It.', 'synopsis' => 'When his peaceful life is threatened by a high-tech assassin, former black-ops agent Frank Moses reassembles his old team in a last ditch effort to survive and uncover his assailants.', 'poster' => '', 'runtime' => 111, 'age_id' => 4],
            ['id' => 3, 'title' => 'Die Hard', 'tagline' => '40 Storeys High - with Suspense, Excitement and Adventure on every level!', 'synopsis' => 'John McClane, officer of the NYPD, tries to save his wife Holly Gennaro and several others that were taken hostage by German terrorist Hans Gruber during a Christmas party at the Nakatomi Plaza in Los Angeles.', 'poster' => '', 'runtime' => 131, 'age_id' => 5],
        ]);
    }
}
