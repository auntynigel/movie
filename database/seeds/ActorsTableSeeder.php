<?php

use Illuminate\Database\Seeder;

class ActorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actors')->insert([
            ['id' => 1, 'forename' => 'Bruce', 'surname' => 'Willis' ],
            ['id' => 2, 'forename' => 'Karl', 'surname' => 'Urban' ],
            ['id' => 3, 'forename' => 'Morgan', 'surname' => 'Freeman' ],
            ['id' => 4, 'forename' => 'John', 'surname' => 'Malkovich' ],
            ['id' => 5, 'forename' => 'Helen', 'surname' => 'Mirren' ],
        ]);
    }
}
