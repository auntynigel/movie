<?php

use Illuminate\Database\Seeder;

class FilmGenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('film_genres')->insert([
            ['id' => 1, 'film_id' => 2, 'genre_id' => 1],
            ['id' => 2, 'film_id' => 2, 'genre_id' => 3],
            ['id' => 3, 'film_id' => 2, 'genre_id' => 1],
            ['id' => 4, 'film_id' => 3, 'genre_id' => 16],
        ]);
    }
}
