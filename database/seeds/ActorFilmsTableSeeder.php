<?php

use Illuminate\Database\Seeder;

class ActorFilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('actor_films')->insert([
            ['id' => 1, 'actor_id' => 1, 'film_id' => 2],
            ['id' => 2, 'actor_id' => 2, 'film_id' => 2],
            ['id' => 3, 'actor_id' => 3, 'film_id' => 2],
            ['id' => 4, 'actor_id' => 4, 'film_id' => 2],
            ['id' => 5, 'actor_id' => 5, 'film_id' => 2],
            ['id' => 6, 'actor_id' => 1, 'film_id' => 3],
        ]);
    }
}
