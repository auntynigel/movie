<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class genre extends Model
{
    public function film(){
        return $this->belongsToMany('App\film', 'film_genres');
    }
}
