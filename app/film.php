<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    public function actor(){
        return $this->belongsToMany('App\actor', 'actor_films');
    }
    
    public function age(){
        return $this->hasOne(age::class, 'id'); // , 'age_films'
    }
    
    public function genre(){
        return $this->belongsToMany('App\genre', 'film_genres');
    }
}
