<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    $author = 'Mark Williams';
    return view('welcome', compact('author'));
});

Route::get('about', function (){
    $author = 'Mark Williams';
    return view('about', compact('author')); // resources/views/about.blade
});

Route::get('movies', 'FilmController@index');
Route::get('movies/{films}', 'FilmController@show');

route::get('add', 'AddController@index');
Route::post('admin/addmovie', 'AddController@store');
