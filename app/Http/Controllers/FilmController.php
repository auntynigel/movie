<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\actor;
use App\age;
use App\film;
use App\genre;

use App\Http\Requests;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $films = film::orderBy('title', 'asc')->get();

        return view('movie', ['films' => $films]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $films = film::find($id);
        $genres = film::find($id)->genre()->get();
        $ages = film::find($id)->age()->get();
        $actors = film::find($id)->actor()->get();

        $run = $films->runtime;

        $hour = floor(($run * 60) / (60 * 60));
        $min = $run % 60;

        // $time = gmdate("H:i",$run * 60);
        
        $title = $films->title;

        return view('movies.bio', ['films' => $films, 'genres' => $genres, 'ages' => $ages, 'actors' => $actors, 'title' => $title, 'hour' => $hour , 'min' => $min]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
