<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\film;

class actor extends Model
{
    public function film(){
        return $this->belongsToMany('App\film', 'actor_films');
    }
}