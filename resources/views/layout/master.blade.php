<!doctype html>
<html lang="en">
<head>
    <meta name="description" content="@yield('description')">
    <meta name="author" content="Mark Williams">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">

    <title>@yield('title')</title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

</head>
<body>
@include('include.header')
@yield('content')
@include('include.footer')

</body>
</html>