@extends('layout.master')
@section('title', 'About')

@section('content')

    <article>
        <div class="row">
            <div class="small-12 medium-6 columns">
                <h1>About {{ $author }}</h1>
                <p>Hi, I'm {{ $author }}. I'm a learning enthusiast, I enjoy learning new skills and further my knowledge for the passion of knowing and learning. Since getting my first computer in 1999, I have self taught myself skills such as early web development and photography. These are my two main passions, apart from the wife that is.</p>
                <p>In 2010 I lost my job due to redundancies, from there after agency jobs and getting no where, it was time to get back into education and pursue a skill-set I thoroughly enjoyed and could lead into a career I would enjoy being in.</p>
                <p></p>
            </div>
            <div class="show-for-medium-up medium-6 columns">
                <img src="{{ url('/images/willow-m.png') }}" alt="Image of author">
            </div>
        </div>
    </article>

@endsection