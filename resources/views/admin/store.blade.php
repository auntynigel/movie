@extends('layout.master')
@section('description', 'Adding new film to Database')
@section('title', 'Add new film')

@section('content')

    <div class="row">
        <div class="small-12 columns">

            <form method="post" action="/admin/addmovie">


                <label for="title">Title
                <textarea name="title" id="title" placeholder="Title of movie"></textarea>
                </label>

                <label for="tagline">Tagline
                <textarea name="tagline" id="tagline" placeholder="Short tagline of movie"></textarea>
                </label>

                <label for="synopsis">Synopsis
                <textarea name="synopsis" id="synopsis" placeholder="Full synopsis of movie"></textarea>
                </label>

                <div class="small-6 columns">
                    <label for="runtime">Movie Runtime
                        <textarea name="runtime" id="runtime" placeholder="Runtime of movie (Minutes)"></textarea>
                    </label>
                </div>

                <div class="small-6 columns">

                    <label for="ageRating">Age Rating
                    <select name="ageRating">
                        @foreach($ages as $age)
                            <option value="{{$age->id}}">{{$age->ageRating}}</option>
                        @endforeach
                    </select>

                    {{--<label>Age Rating--}}
                        {{--<select >--}}

                            {{--<option value="" disabled selected hidden>Please Choose...</option>--}}
                            {{--<option value="1" id="ageRating">Universal.</option>--}}
                            {{--<option value="2" id="AgeRating">Parental Guidance</option>--}}
                            {{--<option value="3" id="ageRating">12+</option>--}}
                            {{--<option value="4" id="ageRating">15+</option>--}}
                            {{--<option value="5" id="ageRating">18+</option>--}}
                            {{--<option value="6" id="ageRating">xxx</option>--}}

                            {{--@foreach($ages as $age)--}}
                                {{--<option value="{{ $age->id }}" id="ageRating">{{ $age->ageRating }}</option>--}}
                            {{--@endforeach--}}

                        {{--</select>--}}
                    {{--</label>--}}
                </div>

                <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                <button type="submit" class="button-round">Add Film</button>
            </form>

        </div>
    </div>


@endsection