@extends('layout.master')
@section('title', 'About')

@section('content')

    <div class="container">
        <div class="content">
            <div class="title">{{ $author }}
                <p class="text-center">Hi, I'm {{ $author }} and I'm a learning enthusiast!</p>
                <p>Within this site is somewhere that I practice and present. For instance, this site was constructed in Laravel 5, film data is stored in a relational database using MySQL and any photography uploaded was most likely created through the use of some digital manipulation in Lightroom or Photoshop.</p>
            </div>
        </div>
    </div>

@endsection
