<header class="header">
    <h1 class="headline">Willows  <small>DESIGN</small></h1>
    <ul class="header-subnav">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li><a href="{{ url('/movies') }}">Movies</a></li>
        <li><a href="{{ url('/about') }}">About</a></li>
    </ul>
</header>