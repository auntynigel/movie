<footer class="footer">
    <div class="row full-width">
        <div class="small-12 medium-3 large-4 columns">
            <i class="fa fa-info"></i>

            <p>My name is Mark and I'm currently a student studying a BSc in Web Design & Development. This incorporates languages such as PHP, JS and Python. As well as studying, I'm married and work adhoc in the <a href="https://www.edgehill.ac.uk/computing/business-support/web-factory/">Web Factory</a>.</p>
        </div>
        <div class="show-for-medium-up medium-3 large-4 columns">
            <i class="fa fa-html5" aria-hidden="true"></i><i class="fa fa-css3" aria-hidden="true"></i><i class="fa fa-github" aria-hidden="true"></i>
<p>I consider myself a fullstack developer, as I am at home on the front end coding from scratch or using frameworks such as foundation and angularJS or behind the scenes with SQL and PHP.</p>
        </div>
        <div class="small-6 medium-3 large-2 columns">
            <h4>Menu</h4>
            <ul class="footer-links">
                <li><a href="{{ url('/') }}">Home</a></li>
                <li><a href="{{ url('/about') }}">About</a></li>
                <li><a href="#">Photos</a></li>
                <li><a href="{{ url('/movies') }}">Movies</a></li>
                <ul>
        </div>
        <div class="small-6 medium-3 large-2 columns">
            <h4>Follow Me</h4>
            <ul class="footer-links">
                <li><a href="https://www.twitter.com/auntynigel" target="_blank" alt="Link to twitter"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                <li><a target="_blank" href="https://www.flickr.com/photos/auntynigel/" alt="Link to Flickr"><i class="fa fa-flickr" aria-hidden="true"></i></a></li>
                <li><a href="https://uk.linkedin.com/in/williamsmark81" target="_blank" alt="Link to Linked in"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
                <ul>
        </div>
    </div>
</footer>