@extends('layout.master')
@section('description', 'Film information for ' . $title)
@section('title', $title)

@section('content')


    <div class="row synopsis">
        <div class="small-12 columns">

            <img class="show-for-medium-up medium-3 columns" src="https://placekitten.com/g/200/300" alt="image placeholder">
            <div class="small-12 medium-9 columns"><h1>{{ $films->title }}</h1></div>
            <div class="small-12 medium-9 columns"><h3>{{ $films->tagline }}</h3></div>

            <div class="small-12 medium-9 columns">
                <p>Rating: {{ $films->age->ageRating }}<br>
                Runtime: {{ $hour }}h {{ $min }}m</p>
            </div>

            <div class="small-12 columns synopsis">{{ $films->synopsis }} </div>
            <div class="small-12 columns actor">
                <h4>Starring:</h4>
                <ul class="no-bullet">
                    @foreach ($actors as $actor)
                        <li>{{ $actor->forename }} {{ $actor->surname }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>



@endsection