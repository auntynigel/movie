@extends('layout.master')
@section('title', 'Films List')

@section('content')

    <section>
        @if (isset ($films))

            <div class="row" id="movies">

                <!-- run through all surveys and list each found -->
                @foreach ($films as $film)
                    <a href="{{ url('movies') }}/{{ $film->id }}" alt="link to film {{ $film->id }}">
                        <div class="small-4 medium-3 columns movie ">
                            <h3>{{ $film->title }}</h3>
                            <p class="show-for-medium-up">{{ $film->tagline }}</p>
                        </div>
                    </a>
                @endforeach

            </div> <!-- end row -->

        @else
            <p>No movies found</p>
        @endif
    </section>

@endsection